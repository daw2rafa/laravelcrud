<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticleTest extends DuskTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/articles');
        $response->assertStatus(200);
        $response->assertSee('Lista de artículos');

    }

    public function testCreate()
    {


        $this->browse(function ($browser) {
            $browser->visit('/articles')
                    ->assertSee('Nuevo')
                    ->press('Nuevo')
                    ->assertPathIs('/articles/create');
        });

    }


}
