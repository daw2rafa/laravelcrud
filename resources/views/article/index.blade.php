@extends('layouts.app')

@section('content')

<div class="container">

<h1>Lista de artículos</h1>

    <table class="table table-bordered">
    <tr>
        <th>Código</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Acciones</th>
    </tr>
    @foreach ($articles as $article)
    <tr>
        <td>{{ $article->code }}</td>
        <td>{{ $article->name }}</td>
        <td>{{ $article->price }}</td>
        <td>
            <a href="/articles/{{ $article->id }}">Ver</a> -
            <a href="/articles/{{ $article->id }}/edit">Editar</a>

            <form method="post" action="/articles/{{ $article->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="delete">
                <input type="submit" value="borrar">
            </form>
        </td>
    </tr>
    @endforeach
</table>

<a href="/articles/create">Nuevo</a>
</div>
@endsection
