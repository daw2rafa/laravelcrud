@extends('layouts.app')

@section('content')

<div class="container">

<h1>Alta de artículo</h1>

<form method="post" action="/articles/{{ $article->id }}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">

    <div  class="form-group">
        <label>Código</label>
        <input class="form-control"  type="text" name="code" value="{{ $article->code }}">
    </div>

    <div class="form-group">
        <label>Nombre</label>
        <input class="form-control"  type="text" name="name" value="{{ $article->name }}">
    </div>

    <div class="form-group">
        <label>Precio</label>
        <input class="form-control"  type="text" name="price" value="{{ $article->price }}">
    </div>

    <div class="form-group">
        <label></label>
        <input class="form-control"  type="submit" name="" value="Nuevo">
    </div>



</form>

</div>
@endsection
