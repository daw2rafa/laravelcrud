@extends('layouts.app')

@section('content')


Editar la foto {{ $id }}

<form method="post" action="/photos/{{ $id }}">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="delete">
    <label>Foto: </label>
    <input type="text" name="photo">
    <br>
    <input type="submit" name="">
</form>

@endsection
