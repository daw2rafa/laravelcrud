# Laravel CRUD

En este proyecto de primeros pasos encontrarás:

- Ejemplos de rutas vistos en clase
- El primer controlador PhotoController asociado a esas rutas
- Un controlador asociado a una ruta resource para realizar un CRUD completo.

## Clonar este proyecto

Si quieres este proyecto debes clonarlo y completar el proceso necesario para cualquier proyecto Laravel:

- Clonar

```
git clone git@bitbucket.org:daw2rafa/laravelcrud.git
```

- Instalar las dependencias con composer

```
composer install
```

- Fichero `.env`.

```
cp .env.example .env
```
- No olvides crear una base de datos y editar este fichero para que pueda usarla

- Generar la clave que Laravel debe usar en todos los procesos de cifrado:

```
php artisan key:generate
```

- Ya puedes ejecutar tu proyecto
```
php artisan migrate   #para crear las tablas
php artisan serve     #para servir el proyecto
```
